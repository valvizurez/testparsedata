//
//  mainContactViewController.swift
//  testParseApp
//
//  Created by Victoria Alvizurez on 10/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class mainContactViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet var tableView: UITableView!
    
    var dataParse:NSMutableArray = NSMutableArray()
    var name = ""
    var valueToPass = ""
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.dataParse.count
    }
    
    @IBAction func logOut(_ sender: Any) {
        PFUser.logOut()
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "loginView") as! loginViewController
                self.present(vc, animated: true, completion: nil)
                
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        //let contacts = self.contact[indexPath.row]
        //cell.textLabel?.text = contacts.name
       // return cell
        let cellDataParse:PFObject = self.dataParse.object(at: indexPath.row) as! PFObject
        cell.textLabel?.text = cellDataParse.object(forKey: "Name") as? String
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow;
        let currentCell = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        valueToPass = (currentCell?.textLabel?.text)!
        self.performSegue(withIdentifier: "moveContact", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "moveContact") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! showNameViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
    }
    
   override func viewWillAppear(_ animated: Bool) {
    self.dataParse.removeAllObjects()
   fetchData()
     // self.tableView.reloadData()
    }
    
    func fetchData(){
        let query = PFQuery(className:"userNames")
        query.whereKey("userId", equalTo:name)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.dataParse.add(object)
                }
                self.tableView.reloadData()
            }
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentUser = PFUser.current()
        name = (currentUser?.username)!
      //  fetchData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
