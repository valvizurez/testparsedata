//
//  newNameViewController.swift
//  testParseApp
//
//  Created by Victoria Alvizurez on 10/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class newNameViewController: UIViewController {
    
    @IBOutlet var newName: UITextField!
  var name  = ""
    
    @objc func buttonTapped(_ sender : UIButton)
    {
    
          var newContact = PFObject(className:"userNames")
            newContact["Name"] = newName.text
            newContact["userId"] = name
            newContact.saveInBackground {
              (success: Bool, error: Error?) in
              if (success) {
                   let _ = self.navigationController?.popViewController(animated: true)
                 // print(success)
              } else {
                var alert = UIAlertView(title: "Error", message: "\(error)", delegate: self, cancelButtonTitle: "OK")
                alert.show()
              }
        
    }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        
        let deleteBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(buttonTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [saveBtn]
        self.navigationItem.leftBarButtonItems = [deleteBtn]
        // Do any additional setup after loading the view.
        
        var currentUser = PFUser.current()
        name = (currentUser?.username)!
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
