//
//  signUpViewController.swift
//  testParseApp
//
//  Created by Victoria Alvizurez on 10/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class signUpViewController: UIViewController {

    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    
    @IBAction func signUp(_ sender: Any) {
        var user = PFUser()
        user.username = userName.text
        user.password = password.text
        // other fields can be set just like with
        
        user.signUpInBackground(block: { (succeed, error) -> Void in
            if ((error) != nil) {
                var alert = UIAlertView(title: "Error", message: "\(error)", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            else {
                var alert = UIAlertView(title: "Success", message: "Signed Up", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                DispatchQueue.global().async {
                    DispatchQueue.main.async {
                        
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "mainContact") as! mainContactViewController
                        let navigationController = UINavigationController(rootViewController: vc)
                        self.present(navigationController, animated: true, completion: nil)
                    }
                }
            }
        })
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        //Sign UP
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
