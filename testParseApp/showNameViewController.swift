//
//  showNameViewController.swift
//  testParseApp
//
//  Created by Victoria Alvizurez on 10/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class showNameViewController: UIViewController {
    
    @IBOutlet var name: UITextView!
    
    var contactName = ""
    var passedValue = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        contactName = passedValue
        name.text = contactName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
